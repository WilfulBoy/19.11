#include <iostream>

class Animal 
{
public:
    virtual void Voice() 
    {
        std::cout << "Some sound!\n";
    }
};

class Dog : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal 
{
public:
    void Voice() override 
    {
        std::cout << "Moo!\n";
    }
};

int main() 
{
    Animal* animals[3] = { new Dog, new Cat, new Cow };
    for (int i = 0; i < 3; ++i) 
    {
        animals[i]->Voice();
        delete animals[i];
    }
    return 0;
}




// 
//template<class T>
//
//class A
//{
//    T value;
//public:
//	A(){}
//	A(T _value): value(_value){}
//	void ShowValue()
//	{
//		std::cout << value;
//	}
//};
//
//
//
//int main()
//{
//	A<int>* ptrInt = new A<int>(10);
//	ptrInt->ShowValue();
//}

//class A
//{
//protected:
//	int x;
//public:
//	A() {}
//	A(int _x) : x(_x)
//	{}
//	int GetX()
//	{
//		return x;
//	}
//	virtual void Show()
//	{
//		std::cout << "A";
//
//	}
//};
//
//class B : public A
//{
//private:
//	double y;
//public:
//	B() {}
//	B(double _y, int _x) : y(_y), A(_x)
//	{}
//	double GetY()
//	{
//		return y;
//	}
//	void Show() override
//	{
//		std::cout << "B";
//  }
//};
//
//int main()
//{
//	A* p = new B(5.2, 2);
//	p->Show();
//}

//#include <iostream>
// 
//class A
//{
//private:
//	int x;
//public:
//	A() {}
//	A(int _x) : x(_x)
//	{}
//	int GetX()
//	{
//		return x;
//	}
//};
//
//class B : public A
//{
//private:
//	double y;
//public:
//	B() {}
//	B(double _y, int _x) : y(_y), A(_x)
//	{}
//	double GetY()
//	{
//		return y;
//	}
//};
//
//int main()
//{
//	B* p = new B(5.2, 2);
//	std::cout << p->GetY()<< ' ' << p->GetX();
//}

//#include <iostream>
//
//class A
//{
//private:
//    int x;
//public:
//    int GetX()
//    {
//        return x;
//    }
//    A()
//    {
//        std::cout << "A cons\n";
//    }
//};
//
//
//
//class B : A
//{
//private:
//    double y;
//public:
//    double GetY()
//    {
//        return y;
//    }
//    B()
//    {
//        std::cout << "B const\n";
//    }
//};
//
//class C : B
//{
//public:
//    C()
//    {
//        std::cout << "C constr\n";
//    }
//};
//
//int main()
//{
//    C* p = new C;
//    
//}

